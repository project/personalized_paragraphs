<?php

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;


/**
 * A helper function that takes a node and creates an array of field names that contain
 * personalized paragraphs.
 * TODO: Likely only works for a PP one level deep; if we try a PP nested 2 or more levels
 * TODO: in a paragraph, it will likely fail.
 * @param $node
 * @return array
 */
function _has_personalized_paragraph($node){
  $fields = [];
  foreach ($node->getFields() as $field_id => $field) {
    $settings = $field->getSettings();
    $has_settings = array_key_exists('handler_settings', $settings);
    if ($has_settings) {
      $has_bundle = array_key_exists('target_bundles', $settings['handler_settings']);
      if ($has_bundle && is_array($settings['handler_settings']['target_bundles'])) {
        foreach ($settings['handler_settings']['target_bundles'] as $id1 => $id2) {
          if ($id1 == 'personalized_paragraph' || $id2 == 'personalized_paragraph') {
            array_push($fields, $field_id);
          }
        }
      }
    }
  }
  return $fields;
}

/**
 * Adds DecisionParagraph::paragraphSubmit() as a custom submit handler to the passed form.
 * paragraphSubmit() is the main entry for saving Decision information related to a personalized paragraph.
 * Note its added at the end of the submit handlers as the PP has not be created yet if its added at the beginning.
 * @param $form
 * @param $plugin
 */
function _add_form_submits(&$form, $plugin){
  $new_submits =
    [
      $plugin,
      'paragraphSubmit'
    ];

  foreach (array_keys($form['actions']) as $action) {
    if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
      $existing_submits = $form['actions'][$action]['#submit'];
          array_push($existing_submits, $new_submits);
        $form['actions'][$action]['#submit'] = $existing_submits;
    }
  }
}

/**
 * Implements hook_form_alter.
 * Executes on every node edit form. If a personalized_paragraph is detected,
 * add the custom form submit required by personalized paragraphs
 * @param $form
 * @param FormStateInterface $form_state
 * @param $form_id
 */
function personalized_paragraphs_form_node_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id){
  $node = $form_state->getFormObject()->getEntity();
  $personalized_fields = _has_personalized_paragraph($node);
  if(!empty($personalized_fields)){
    if ($plugin = personalized_paragraphs_get_handler('personalized_paragraph')) {
      _add_form_submits($form, $plugin);
    }
  }
}


/**
 * If the personalized paragraph has a decision token,
 * load the decision form with this information otherwise load an empty decision form. The parent
 * Paragraph's unique machine name is passed along in configuration values (see DecisionParagraph::paragraphForm)
 * in the plugin so it can be used to set the #name attribute of the button that retrieves
 * reactions; this enables Drupal core to match the element that triggered the
 * ajax submission to an actual element on the page.
 * TODO: this might mess up if there is ever more than one PP per Paragraph
 * @param $plugin
 * @param $element
 * @param $context
 * @param $form_state
 * @param $decision_token
 */
function _attach_decision_edit_form(&$plugin, &$element, $context, &$form_state, $decision_token) {

    if($decision_token){
      $plugin->loadDecisionByToken($decision_token);
    }
//  This allows us to include the unique machine name of the parent paragraph as part of
//  the name attribute of the 'Select Segment Set' button which allows Drupal core
//  To match the triggering element to the button that triggered it during ajax form processing.
//  The name is set in MultipleParagraphDecision::processSegmentSetSelectWidget
    $parent_field = $context['items']->getName();
    $plugin->setConfigurationValue('parent', $parent_field);
    $build_form = $plugin->buildConfigurationForm([], $form_state);
    $element['subform']['smart_content'] = $build_form;
}

/**
 * Implements hook_form_alter
 * This hook runs for every Paragraph on the Node edit page and loads decision information
 * for each found personalized_paragraph.
 * @param $element
 * @param FormStateInterface $form_state
 * @param $context
 */
function personalized_paragraphs_field_widget_entity_reference_paragraphs_form_alter(&$element, \Drupal\Core\Form\FormStateInterface &$form_state, $context){
  $type = $element['#paragraph_type'];
  if($type == 'personalized_paragraph'){
    //Remove access to the decision_content_token_field
    $parents = ['subform', 'field_decision_content_token'];
    $decision_token_field = NestedArray::getValue($element, $parents);
    if($decision_token_field){
      $element['subform']['field_decision_content_token']['#access'] = FALSE;
    }
    //Load the PP plugin
    if ($plugin = personalized_paragraphs_get_handler('personalized_paragraph')) {
      $parents = ['subform', 'field_decision_content_token', 'widget', 0, 'value', '#default_value'];
      $decision_token = NestedArray::getValue($element, $parents);
      _attach_decision_edit_form($plugin, $element, $context, $form_state, $decision_token);
    }
  }
}

/**
 * Implements hook_preprocess_hook.
 * The main hook that loads a decision paragraph on page view.
 * Detects if theme variables contain a personalized_paragraph and if so checks for a non-empty
 * decision_content_token. It calls the DecisionParagraph::build() method to create the
 * arrays smart_content.js needs to pick the winning Reaction. Additionally it adds
 * a 'decision_paragraphs' key and content to drupalSettings; this can be used to determine the
 * winner (or lack thereof) in javascript and execute JS code.
 * @param $variables
 */
function personalized_paragraphs_preprocess_field__entity_reference_revisions(&$variables){
  //A personalized paragraph will only ever have 1 item reference
  //This will break if it somehow ever has more
  $parents = ['items', 0, 'content', '#paragraph'];
  $para = NestedArray::getValue($variables, $parents);
  if($para->bundle() == 'personalized_paragraph'){
    if ($plugin = personalized_paragraphs_get_handler('personalized_paragraph')) {
      $has_token = !$para->get('field_decision_content_token')->isEmpty();
      if($has_token) {
        $token = $para->get('field_decision_content_token')->getValue()[0]['value'];
        $build = $plugin->build($token);
        $has_attached = array_key_exists('#attached', $build);
        if ($has_attached && !empty($build['#attached']['drupalSettings']['smartContent'])) {
          $merged = array_merge($variables['items'][0]['content'], $build);
          $variables['items'][0]['content'] = $merged;

          $para_data = [
            'token' => $token,
          ];
          $has_name = !$para->get('field_machine_name')->isEmpty();
          $name = $has_name ? $para->get('field_machine_name')->getValue()[0]['value'] : '';
          $variables['items'][0]['content']['#attached']['drupalSettings']['decision_paragraphs'][$name] = $para_data;
        } else {
        //The build failed, display default paragraph?
          \Drupal::logger('personalized_paragraphs')->notice("Personalized Paragraph: ".$para->id()." failed to build token: ".$token);
        }
      } else {
        //In here if somehow a paragraph never has gotten decision_content_token
        //Perhaps consider loading a default paragraph?
        \Drupal::logger('personalized_paragraphs')->notice("Personalized Paragraph: ".$para->id()." did not contain a decision_content_token on load.");
      }
    }
  }
}


/**
 * Get the correct ParagraphHandler plugin for a given paragraph.
 *
 * @param Drupal\paragraphs\Entity\Paragraph $paragraph
 *   A paragraph entity.
 *
 * @return mixed
 *   Null or a ParagraphHandler plugin.
 */
function personalized_paragraphs_get_handler($plugin_name) {
  $plugin_manager = \Drupal::service('plugin.manager.personalized_paragraphs');
  $definitions = $plugin_manager->getDefinitions();

  foreach ($definitions as $plugin_id => $definition) {
    if ($plugin_id == $plugin_name) {
      return $plugin_manager->createInstance($plugin_id);
    }
  }
  return false;
}

/**
 * Creates an instance of the DecisionParagraph plugin with the passed token
 * and deletes the decision content
 * @param $token
 */
function _delete_decision_content($token){
  if ($plugin = personalized_paragraphs_get_handler('personalized_paragraph')) {
    $plugin->loadDecisionByToken($token);
    $plugin->getDecisionStorage()->delete();
  }
}

/**
 * Implements hook_entity_predelete.
 * Detects the deletion of a node that has personalized_paragraphs.
 * If the paragraph has a decision_content_token, load the decision and delete it. Making note here of the
 * fact that paragraphs dont get deleted in a straightforward way; they are added to a Queue and 'marked' for
 * deletion (deleted on queue run). I was told by others that you can count on these being deleted and that
 * they can never be restored, so I felt it okay to go ahead and delete the decision content.
 * @param EntityInterface $entity
 */
function personalized_paragraphs_entity_predelete(EntityInterface $entity)
{
  if ($entity instanceof Node) {
    if ($fields = _has_personalized_paragraph($entity)) {
      foreach ($fields as $field) {
        $has_para = $entity->get($field)->referencedEntities();
        if (!empty($has_para)) {
          $has_token = !$has_para[0]->get('field_decision_content_token')->isEmpty();
          if ($has_token) {
            $token = $has_para[0]->get('field_decision_content_token')->getValue()[0]['value'];
            _delete_decision_content($token);
          }
        }
      }
    }
  }
}
