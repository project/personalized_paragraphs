CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Security
 * Maintainers
 * Special Thanks


INTRODUCTION
------------

Personalized Paragraphs is a Drupal module that enables
the displaying of personalized content to anonymous users using Paragraphs.
Personalized Paragraphs depends heavily on the Smart Content module
and the Paragraphs module. One might think of Personalized Paragraphs
as a 'port' of the Smart Content Block sub-module to Paragraphs.

For more information on how the module was built, visit
Personalized Paragraphs: Porting Smart Content to Paragraphs for Drupal:
https://pierce-lamb.medium.com/personalized-paragraphs-porting-smart-content-to-paragraphs-for-drupal-46b9c8e35e43

For more information on how to use the module, visit
How To Use Personalized Paragraphs:
https://pierce-lamb.medium.com/how-to-use-personalized-paragraphs-559dd5e76775

For a full description of the module, visit the project page:
https://www.drupal.org/project/personalized_paragraphs

Links to the modules Personalized Paragraphs depends on:
- https://www.drupal.org/project/smart_content
- https://www.drupal.org/project/paragraphs
- https://www.drupal.org/project/entity_usage

REQUIREMENTS
------------

Smart Content, Paragraphs and Entity Usage will be installed
with this module.

- https://www.drupal.org/project/smart_content
- https://www.drupal.org/project/paragraphs
- https://www.drupal.org/project/entity_usage


RECOMMENDED MODULES
-------------------

- Smart Content UTM
  (https://www.drupal.org/project/smart_content_utm)
  provides conditions for handling UTM strings. UTM values are
  automatically cookied and made available as conditions to use in segments.

- Smart Content Demandbase
  (https://www.drupal.org/project/smart_content_demandbase)
  is a connector module that allows data available through Demandbase’s API
  to be used as conditions. Note: this module requires an active Demandbase subscription.

- Smart Content 6sense
  (https://www.drupal.org/project/smart_content_6sense)
  is a connector module that allows data available through 6sense's API to
  be used as conditions. Note: this module requires an active 6sense subscription.

- Smart Content Marketo RTP
  (coming soon)
  is a connector module that allows data available through Marketo RTP's API
  to be use in conditions. Note: this module requires and active Marketo RTP subscription.

- Smart Content Datalayer
  (https://www.drupal.org/project/smart_content_datalayer)
  Collects data from smart content displayed on a page and provides it to analytics
  platforms like Google Tag Manager via the dataLayer object.



INSTALLATION
------------

- Follow these instructions for a typical Drupal module installation:
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

CONFIGURATION
-------------

Please visit the 'How To Use Personalized Paragraphs' blog for more
information on configuration:
https://pierce-lamb.medium.com/how-to-use-personalized-paragraphs-559dd5e76775

You may find the Personalized Paragraphs: Porting Smart Content to Paragraphs for Drupal
blog useful as well:
https://pierce-lamb.medium.com/personalized-paragraphs-porting-smart-content-to-paragraphs-for-drupal-46b9c8e35e43


SECURITY
--------

Please see the Smart Content module's comments on security. You can view
them here:

https://git.drupalcode.org/project/smart_content/-/blob/3.x/README.txt

MAINTAINERS
-----------

Current maintainers:

  - Pierce Lamb (plambie); Primary Developer
  https://www.drupal.org/u/plambie

This project has been sponsored by:

  - TIBCO
    TIBCO makes it possible to unlock the potential of your
    real-time data for making faster, smarter decisions.

SPECIAL THANKS
--------------

  * Michael Lander (michaellander); Smart Content Maintainer
  https://www.drupal.org/u/michaellander