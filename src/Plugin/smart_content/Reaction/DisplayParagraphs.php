<?php

namespace Drupal\personalized_paragraphs\Plugin\smart_content\Reaction;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\smart_content\Cache\CacheableAjaxResponse;
use Drupal\smart_content\Decision\PlaceholderDecisionInterface;
use Drupal\smart_content\Reaction\ReactionConfigurableBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides 'Paragraph' display reaction.
 *
 * @SmartReaction(
 *  id = "display_paragraphs",
 *  label = @Translation("Paragraph"),
 * )
 */
class DisplayParagraphs extends ReactionConfigurableBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type manager service.
   *
   */
  protected $entityTypeManager;


  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The paragraphs that are a part of this reaction.
   *
   * @var array
   */
  protected $paragraphs = [];

  /**
   * DisplayParagraphs constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The context repository service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user account.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ContextRepositoryInterface $contextRepository, AccountInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->contextRepository = $contextRepository;
    $this->currentUser = $currentUser;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('context.repository'),
      $container->get('current_user')
    );
  }

  /**
   * Heavily modified from DisplayBlocks::buildConfigurationForm. I removed the initial ceremony
   * of dismissing certain block types and instead query paragraphs_library_items to get all
   * existing Library items. These combine to form the select dropdowns in Reactions. I've left
   * the code that displays "Add" buttons and their handlers commented as we may use it later.
   * Reactions now function by storing what ever value was selected in the select dropdown vs
   * using the "Add" buttons
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $pg_lib_conn = $this->entityTypeManager->getStorage('paragraphs_library_item');
    $paragraphs = $pg_lib_conn->loadMultiple();
    $options = [];
    $options[''] = "- Select a Paragraph -";
    foreach($paragraphs as $paragraph){
      $maybe_parent = $paragraph->get('paragraphs')->referencedEntities();
      if(!empty($maybe_parent)) {
        $parent_name = $maybe_parent[0]->bundle();
        $options[$parent_name][$paragraph->id()] = $paragraph->label();
      } else {
        $options[$paragraph->id()] = $paragraph->label();
      }
    }

    $wrapper_id = Html::getUniqueId('paragraph-settings-wrapper');
    $form['label'] = [
      '#type' => 'markup',
      '#prefix' => '<div class="form-item-label">',
      '#suffix' => '</div>',
      '#markup' => $this->t('Display the following paragraphs'),
    ];

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'id' => $wrapper_id . '-paragraphs',
        'class' => [
          'block-reaction',
          'reaction-settings-wrapper',
        ],
      ],
      '#title' => $this->t('Configure Paragraphs to Display'),
    ];

    $form['settings']['add_paragraph'] = [
      '#type' => 'container',
      '#title' => $this->t('Add Paragraph'),
      '#attributes' => [
        'class' => ['block-add-container', 'add-container'],
      ],
    ];

    $form['settings']['add_paragraph']['paragraph_type'] = [
      '#title' => $this->t('Paragraph Type'),
      '#title_display' => 'invisible',
      '#type' => 'select',
      '#options' => $options,
      '#empty_value' => '',
      '#empty_option' => $this->t('- Select a Paragraph -'),
      ];

    $existing_paragraphs = $this->getParagraphs([]);
    if (empty($existing_paragraphs)) {
//      $form['settings']['blocks'] = [
//        '#type' => 'markup',
//        '#markup' => $this->t('No paragraphs set to display.'),
//        '#prefix' => '<div class="messages messages--warning">',
//        '#suffix' => '</div>',
//      ];
      $form['settings']['add_paragraph']['paragraph_type']['#empty_option'] = $this->t('- Select a Paragraph -');

    }
    else {
      //Get the id of the selected paragraph
      $selected_id = reset($existing_paragraphs)['id'];
      $form['settings']['add_paragraph']['paragraph_type']['#default_value'] = $selected_id;
//      $form['settings']['blocks'] = [
//        '#type' => 'table',
//        '#header' => [
//          $this->t('Configuration'),
//          $this->t('Operations'),
//          $this->t('Weight'),
//        ],
//        '#tabledrag' => [
//          [
//            'action' => 'order',
//            'relationship' => 'sibling',
//            'group' => $wrapper_id . '-order-weight',
//          ],
//        ],
//      ];
//
//      // Initialize default weight variable.
//      $i = 0;
//      // Loop through each block and add it to the form.
//      foreach ($existing_paragraphs as $segment_id => $para_config) {
//        // Configuration.
//        $form['settings']['blocks'][$segment_id]['plugin_form'] = $para_config;
//        $form['settings']['blocks'][$segment_id]['plugin_form']['#type'] = 'container';
//        $form['settings']['blocks'][$segment_id]['plugin_form']['#attributes']['class'][] = 'reaction-item-settings-wrapper';
//        $form['settings']['blocks'][$segment_id]['#attributes']['class'][] = 'draggable';
//
//        // Operations.
//        $form['settings']['blocks'][$segment_id]['remove'] = [
//          '#type' => 'submit',
//          '#value' => $this->t('Remove block'),
//          '#submit' => [[$this, 'removeElementBlock']],
//          '#attributes' => [
//            'class' => [
//              'align-right',
//              'remove-condition',
//              'remove-button',
//            ],
//          ],
//          '#limit_validation_errors' => [],
//          '#ajax' => [
//            'callback' => [$this, 'removeElementBlockAjax'],
//            'wrapper' => $wrapper_id . '-blocks',
//          ],
//        ];
//
//        // Weight.
//        $form['settings']['blocks'][$segment_id]['weight'] = [
//          '#type' => 'weight',
//          '#title' => $this->t('Weight'),
//          '#title_display' => 'invisible',
//          '#default_value' => $i,
//          '#attributes' => ['class' => [$wrapper_id . '-order-weight']],
//        ];
//        $i++;
//      }
    }



//    //TODO: this code displays the add paragraph button, but of course it doesn't display
    // it appears when its outside of 'add_paragraph' but disappears inside.
    // may be because add_paragraph ends up being a select?
    $form['settings']['add_paragraph']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Paragraph'),
      '#validate' => [[$this, 'addElementBlockValidate']],
      '#limit_validation_errors' => [],
      '#submit' => [[$this, 'addElementBlock']],
      '#ajax' => [
        'callback' => [$this, 'addElementBlockAjax'],
        'wrapper' => $wrapper_id . '-blocks',
      ],
    ];
    $form['#process'][] = [$this, 'buildWidget'];
    return $form;
  }

  /**
   * Process callback to attach unique id's based on parents.
   * Commented the Remove button since we aren't using the AJAX buttons with reactions
   */
  public function buildWidget($element, FormStateInterface $form_state, array $form) {
    $unique_id = Html::getClass(implode('-', $element['#parents']));

    // Add unique names for the remove buttons.
//    foreach ($this->getParagraphs([]) as $block_id => $block) {
//      if ($block instanceof PluginFormInterface) {
//        $element['settings']['blocks'][$block_id]['remove']['#name'] = 'remove_block_' . $unique_id . '__' . $block_id;
//      }
//    }

    // Get the "Add block" button ready for AJAX.
    $element['settings']['add_paragraph']['submit'] = [
      '#name' => 'add-paragraph-' . $unique_id,
      '#limit_validation_errors' => [
        array_merge($element['#array_parents'], [
          'settings',
          'add_paragraph',
          'paragraph_type',
        ]),
      ],
    ];

    return $element;
  }

  /**
   * Provides a '#validate' callback for adding a Block.
   *
   * Validates that a valid block type is selected.
   */
  public function addElementBlockValidate(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $array_parents = array_slice($button['#array_parents'], 0, -1);
    $parents = array_slice($button['#parents'], 0, -1);
    $parents[] = 'paragraph_type';
    $array_parents[] = 'paragraph_type';
    if (!$value = NestedArray::getValue($form_state->getUserInput(), $parents)) {
      $form_state->setError(NestedArray::getValue($form, $array_parents), 'Block type required.');
    }
  }

  /**
   * Provides a '#submit' callback for adding a Block.
   */
  public function addElementBlock(array &$form, FormStateInterface $form_state) {
    if (!$form_state->isValidationComplete()) {
      return;
    }
    $button = $form_state->getTriggeringElement();

    // Map block weights to collection.
    $all_values = NestedArray::getValue($form_state->getUserInput(), array_slice($button['#parents'], 0, -3));
    $this->mapFormBlocksWeight($all_values);

    $type = NestedArray::getValue($form_state->getUserInput(), array_slice($button['#parents'], 0, -1))['block_type'];
    $block = $this->blockManager->createInstance($type);
    $this->getBlocksPluginCollection()->add($block);
    $form_state->setRebuild();
  }

  /**
   * Provides an '#ajax' callback for adding a Block.
   */
  public function addElementBlockAjax(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));
  }

  /**
   * Provides a submit callback for removing a block.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function removeElementBlock(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Map block weights to collection.
    $all_values = NestedArray::getValue($form_state->getUserInput(), array_slice($button['#parents'], 0, -4));
    $this->mapFormBlocksWeight($all_values);

    [$action, $name] = explode('__', $button['#name']);
    $this->getBlocksPluginCollection()->removeInstanceId($name);
    $form_state->setRebuild();
  }

  /**
   * Provides an AJAX callback for removing a block.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return mixed
   *   The AJAX response.
   */
  public function removeElementBlockAjax(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    return NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -3));
  }

  /**
   * Edited from DisplayBlocks::validateConfigurationForm. Gets data using ->getUserInput()
   * instead of PluginCollections; throws a validation error if no Paragraph has been selected
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $field_name = $this->getConfiguration()['parent_field'];
    $segments = $this->getParagraphs($form_state->getUserInput());
    if(!empty($segments) && array_key_exists($this->getSegmentDependencyId(), $segments[$field_name])) {
      $current_segment = $segments[$field_name][$this->getSegmentDependencyId()];
      $parents = ['settings', 'reaction_settings', 'plugin_form', 'settings', 'add_paragraph', 'paragraph_type'];
      $pid = NestedArray::getValue($current_segment, $parents);
      if (empty($pid)) {
        //Shows that at least one reaction had no paragraph selected
        //throw validation error
        $form_state->setError($form, t('No paragraph selected'));
      }
    }

  }

  /**
   * Heavily modified from DisplayBlocks:submitConfigurationForm. Gets form values from
   * ->getUserInput() and stores the selected value in an instance variable and in config.
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $field_name = $this->getConfiguration()['parent_field'];
    $segments = $this->getParagraphs($form_state->getUserInput());
    if(!empty($segments) && array_key_exists($this->getSegmentDependencyId(), $segments[$field_name])) {
      $current_segment = $segments[$field_name][$this->getSegmentDependencyId()];
      $parents = ['settings', 'reaction_settings', 'plugin_form', 'settings', 'add_paragraph', 'paragraph_type'];
      $pid = NestedArray::getValue($current_segment, $parents);

      $this->setConfiguration($this->getConfiguration());

      if (!empty($pid)) {
        $options_arr = $form['settings']['add_paragraph']['paragraph_type']['#options'];
        $paragraph_title = '';
        //Searches over the options, returning the title of the selected paragraph
        foreach ($options_arr as $type => $options) {
          if (array_key_exists($pid, $options)) {
            $paragraph_title = $options[$pid];
            break;
          }
        }
        //Will need to modify below if this ever supports multiple paragraphs for one reaction
        //if the re-assigning of this->paragraphs to an empty array is removed, it will
        //append a new paragraph array to this->paragraphs every time the node form is submitted
        $this->paragraphs = [];
        $this->paragraphs[] = [
          'id' => $pid,
          'label' => $paragraph_title,
          'provider' => 'paragraphs_library',
          'label_display' => 'visible',
          'status' => true,
          'info' => "",
          'view_mode' => 'full'
        ];
        $configuration = [
          'id' => $this->getPluginId(),
          'segment_id' => $this->getSegmentDependencyId(),
          'paragraphs' => $this->paragraphs,
        ];
        $this->setConfiguration($configuration);
      }
    }
  }

  /**
   * Maps the block weight values to the block collection.
   *
   * @param array $values
   *   The base form values or input.
   */
  public function mapFormBlocksWeight(array $values) {
    if (!empty($values['settings']['blocks'])) {
      $block_values = $values['settings']['blocks'];
      if (count($block_values) > 1) {
        $keys = array_keys($block_values);
        $this->getBlocksPluginCollection()->mapWeightValues($keys);
      }
    }
  }


  /**
   * Get paragraphs from form input. Heavily modified from DisplayBlocks::getBlocks()
   *
   * @return array
   *   An array of block instances.
   */
  protected function getParagraphs($user_input) {
    $paragraphs = [];
    if(empty($user_input)) {

      $config = $this->getConfiguration();
      if(array_key_exists('paragraphs', $config) && $config['paragraphs']) {
        foreach ($config['paragraphs'] as $para) {
          array_push($paragraphs, $para);
        }
      }

    }else {
      if (array_key_exists('parent_field', $this->getConfiguration())) {
        $field_name = $this->getConfiguration()['parent_field'];
        $widget_state = $user_input[$field_name];
        $filter_widget = array_filter(
          $widget_state,
          function ($key) {
            return is_numeric($key);
          },
          ARRAY_FILTER_USE_KEY
        );
        $digit = array_key_first($filter_widget);
        $parents = [$digit, 'subform', 'smart_content', 'decision', 'decision_settings', 'segments'];
        $reaction_settings = NestedArray::getValue($user_input[$field_name], $parents);
        $reaction_arr = $reaction_settings[$this->getSegmentDependencyId()];
        $paragraphs[$field_name][$this->getSegmentDependencyId()] = $reaction_arr;
      }
    }
    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = $this->configuration + parent::getConfiguration();
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    parent::setConfiguration($configuration);
    $configuration = $configuration + $this->defaultConfiguration();
    if (isset($configuration['paragraphs'])) {
      // TODO: Can we just use the collection, since it takes care of both
      // config and instances? Initialize collection in the constructor and
      // here, just add config to it.
      $this->paragraphs = $configuration['paragraphs'];
      $this->paragraphsPluginCollection = NULL;
    }
    $this->configuration = $configuration;
  }

  public function setConfigurationValue($key, $value) {
    $this->configuration[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
      //'blocks' => [],
    ];
  }


  /**
   * Modified from DisplayBlocks::getResponse
   * {@inheritdoc}
   */
  public function getResponse(PlaceholderDecisionInterface $decision) {
    $response = new CacheableAjaxResponse();
    $content = [];
    // Load all the blocks that are a part of this reaction.
    $paragraphs = $this->getParagraphs([]);
    if (!empty($paragraphs)) {
      // Build the render array for each block.
      foreach ($paragraphs as $para_arr) {
        $pg_lib_conn = $this->entityTypeManager->getStorage('paragraphs_library_item');
        $para_lib_item = $pg_lib_conn->load($para_arr['id']);
        $has_para = !$para_lib_item->get('paragraphs')->isEmpty();
        if($has_para){
          $para_id = $para_lib_item->get('paragraphs')->getValue();
          $target_id = $para_id[0]['target_id'];
          $target_revision_id = $para_id[0]['target_revision_id'];
          $para = Paragraph::load($target_id);
          $render_arr = $this->entityTypeManager->getViewBuilder('paragraph')->view($para);
          $access = $para->access('view',$this->currentUser, TRUE);
          $response->addCacheableDependency($access);
          if ($access) {
            $content[] = [
              'content' => $render_arr
            ];
            $response->addCacheableDependency($render_arr);
          }
        }

      }
    }
    // Build and return the AJAX response.
    $selector = '[data-smart-content-placeholder="' . $decision->getPlaceholderId() . '"]';
    $response->addCommand(new ReplaceCommand($selector, $content));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function getPlainTextSummary() {
    $paragraphs = $this->getParagraphs([]);
    if (empty($paragraphs)) {
      return '';
    }

    $labels = array_map(function ($paragraph) {
      return $paragraph['label'];
    }, $paragraphs);
    return implode(', ', $labels);
  }

  /**
   * {@inheritDoc}
   */
  public function getHtmlSummary() {
    $paras = $this->getParagraphs([]);
    $paras_summary = [];
    if (!empty($paras)) {
      foreach ($paras as $para) {
        $paras_summary[] = [
          '#markup' => '<p>' . $para['label'] . '</p>',
        ];
      }
    }
    else {
      $paras_summary[] = [
        '#markup' => '<p>No blocks selected</p>',
      ];
    }
    return [
      '#prefix' => '<div class="reaction--display-blocks">',
      '#suffix' => '</div>',
      'markup' => [
        '#markup' => '<p><em>Display the following blocks: </p></em>',
      ],
      'blocks' => [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $paras_summary,
      ],
    ];
  }


}
