<?php

namespace Drupal\personalized_paragraphs\Plugin\PersonalizedParagraph;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\personalized_paragraphs\Plugin\PersonalizedParagraphsBase;
use Drupal\smart_content\Decision\DecisionManager;
use Drupal\smart_content\Decision\Storage\DecisionStorageBase;
use Drupal\smart_content\Decision\Storage\DecisionStorageManager;
use Drupal\smart_content\Decision\Storage\RevisionableParentEntityUsageInterface;
use Drupal\smart_content\Form\SegmentSetConfigEntityForm;
use Drupal\smart_content\Plugin\smart_content\Decision\Storage\ConfigEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Decision Paragraph.
 *
 * @package Drupal\personalized_paragraphs\Plugin\PersonalizedParagraph
 *
 * @PersonalizedParagraph(
 *   id = "personalized_paragraph",
 *   label = @Translation("Personalized Paragraph")
 * )
 */

class DecisionParagraph extends PersonalizedParagraphsBase implements ContainerFactoryPluginInterface {
  /**
   * The decision storage.
   *
   * @var mixed
   */
  protected $decisionStorage;

  /**
   * The decision storage plugin manager.
   *
   * @var \Drupal\smart_content\Decision\Storage\DecisionStorageInterface
   */
  protected $decisionStorageManager;

  /**
   * The decision plugin manager.
   *
   * @var \Drupal\smart_content\Decision\DecisionManager
   */
  protected $decisionManager;

  /**
   * Constructs a DecisionParagraph object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\smart_content\Decision\DecisionManager $decisionManager
   *   The decision plugin manager.
   * @param \Drupal\smart_content\Decision\Storage\DecisionStorageManager $decisionStorageManager
   *   The decision storage manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DecisionManager $decisionManager, DecisionStorageManager $decisionStorageManager) {
    $this->decisionManager = $decisionManager;
    $this->decisionStorageManager = $decisionStorageManager;

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (!$this->getDecisionStorage()->hasDecision()) {
      $decision_stub = $this->decisionManager->createInstance('multiple_paragraph_decision');
      $this->getDecisionStorage()->setDecision($decision_stub);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.smart_content.decision'),
      $container->get('plugin.manager.smart_content.decision_storage')
    );
  }

  /**
   * Custom method I added. Utilizes ContentEntity::loadDecisionFromToken to get a decision
   * based on the passed token and assign it to decision storage.
   * @param $token
   */
  public function loadDecisionByToken($token){
    $new_decision = $this->getDecisionStorage()->loadDecisionFromToken($token);
    //$new_decision->setDecision($this->decisionStorage->getEntity()->getDecision());
    $this->decisionStorage = $new_decision;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'decision_storage' => [],
    ];
  }

  /**
   * Slightly modified from DecisionBlock::build(); takes a decision_content_token
   * as a parameter and loads a decision by calling a helper that ultimately calls
   * ContentEntity::loadDecisionFromToken. The result is assigned to decision storage
   * and then the decision storage build method is called. This builds the render array(s)
   * necessary for loading a decision paragraph on a page.
   * {@inheritdoc}
   */
  public function build($token) {
    $this->loadDecisionByToken($token);
    $storage = $this->getDecisionStorage();
    if ($storage->hasDecision() && $storage->getDecision()->getSegmentSetStorage()->getPluginId() !== 'broken') {
      $decision = $this->getDecisionStorage()->getDecision();
      $build = [
        '#attributes' => ['data-smart-content-placeholder' => $decision->getPlaceholderId()],
        '#markup' => ' ',
      ];
      $build = $decision->attach($build);
    }
    else {
      $build = ['#markup' => ''];
    }

    return $build;
//    $decision = $this->getDecisionStorage()->getDecision();
//
//    $build = [
//      '#attributes' => ['data-smart-content-placeholder' => $decision->getPlaceholderId()],
//      '#markup' => ' ',
//    ];
//
//    $build = $decision->attach($build);
//    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function buildPreview() {
    return [
      '#markup' => $this->t('Placeholder for "@label" decision paragraph', ['@label' => $this->label()])
    ];
  }

  /**
   * Slightly modified from DecisionBlock::blockSubmit(). The parent Paragraph's unique
   * machine name is passed through configuration values (from _attach_decision_edit_form in pp.module) then passed from here to
   * MultipleParagraphDecision to be used in assigning the #name attribute for the
   * Select Segment Set button (this enables ajax to work correctly. Longer comment in pp.module)
   * {@inheritdoc}
   */
  public function paragraphForm($form, FormStateInterface $form_state) {
    $form = parent::paragraphForm($form, $form_state);
    if(array_key_exists('parent', $this->getConfiguration())) {
      $parent_field = $this->getConfiguration()['parent'];
      $this->getDecisionStorage()->getDecision()->setConfigurationValue('parent_field', $parent_field);
    }
    $form['#process'][] = [$this, 'buildWidget'];
    $form['#attached']['library'][] = 'smart_content/form';
    return $form;
  }

  /**
   * Render API callback: builds the formatter settings elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   * @param array $complete_form
   *   The complete form array.
   *
   * @return array
   *   The processed form element.
   */

  public function buildWidget(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if ($decision_storage = DecisionStorageBase::getWidgetState($element['#array_parents'], $form_state)) {
      $this->decisionStorage = $decision_storage;
    }
    DecisionStorageBase::setWidgetState($element['#array_parents'], $form_state, $this->getDecisionStorage());
    SegmentSetConfigEntityForm::pluginForm($this->getDecisionStorage()
      ->getDecision(), $element, $form_state, ['decision']);
    return $element;
  }

  //Gets all personalized paragraph outer field names
  //TODO: this can be a lot better
  private function getFieldByKey($key, $array) {
    $personalized_fields = [];
    if(is_array($array)) {
      foreach ($array as $field => $first_arr) {
        if (is_array($first_arr)) {
          foreach ($first_arr as $first_key => $first_value) {
            if (is_array($first_value)) {
              foreach ($first_value as $second_key => $second_value) {
                if ($second_key == 'subform' && is_array($second_value)) {
                  foreach ($second_value as $main_key => $main_value) {
                    //echo '<pre>Curr key: '.(string) $main_key.'</pre>';
                    if ($main_key == $key) {
                      array_push($personalized_fields, $field);
                    }
                  }
                  break;
                }
              }
            }
          }
        }
      }
    }
    return $personalized_fields;
  }

  /**
   * Heavily modified from DecisionBlock::blockSubmit(). We depart from block collections for
   * storing form data and get the raw ->getUserInput() data. With this we iterate the form fields
   * looking for personalized paragraphs. We then iterate the personalized paragraphs extracting
   * the decision storage data, saving it and setting the returned token to the Personalized
   * Paragraph's decision_content_token field. This will be used later to load the correct
   * decision when the page is viewed. We detect if the saved entity is not a default revision
   * and correctly setNewRevision on the decision content. Finally, we ->addUsage on the
   * saved decision content so usage can be tracked.
   * {@inheritdoc}
   */
  public function paragraphSubmit($form, FormStateInterface $form_state) {
    //Loop over the field arrays, finding only those fields with personalized paragraphs
    $filtered = array_filter(
      $form_state->getUserInput(),
      function ($key) {
        return str_starts_with($key, 'field_');
      },
      ARRAY_FILTER_USE_KEY
    );

    $is_personalized = $this->getFieldByKey('smart_content', $filtered);
    if(!empty($is_personalized)) {
      foreach($is_personalized as $field_name) {
        //Get the numerical value of the item contained in 'widget'
        //This always has a numerical key and nothing else in 'widget' is numerical.
        //TODO: array_key_first will break if there is ever more than 1 personalized paragraph in the widget.
        $widget_state = $form[$field_name]['widget'];
        $filter_widget = array_filter(
          $widget_state,
          function ($key) {
            return is_numeric($key);
          },
          ARRAY_FILTER_USE_KEY
        );
        $digit = array_key_first($filter_widget);

        $parents = [$field_name, 'widget', $digit, 'subform', 'smart_content'];
        if ($decision_storage = DecisionStorageBase::getWidgetState($parents, $form_state)) {
          if ($decision_storage instanceof ConfigEntity) {
            $decision = clone $decision_storage->getDecision();
            $decision_storage = $this->decisionStorageManager->createInstance('content_entity');
            $decision_storage->setDecision($decision);
          }
          $this->decisionStorage = $decision_storage;
        }

        if (isset($form['decision'])) {
          $element = $form;
        } else {
          $element = NestedArray::getValue($form, $parents);
        }

        if ($element) {
          // Get the decision from storage.
          $decision = $this->getDecisionStorage()->getDecision();
          if ($decision->getSegmentSetStorage()) {
            // Submit the form with the decision.
            SegmentSetConfigEntityForm::pluginFormSubmit($decision, $element, $form_state, ['decision']);
            // Set the decision to storage.
            $this->getDecisionStorage()->setDecision($decision);
          }
        } else {
          \Drupal::logger('personalized_paragraphs')->notice("DecisionParagraph::paragraphSubmit unable to acquire the smart_content element");
        }
        if ($this->getDecisionStorage()) {
          $node = $form_state->getFormObject()->getEntity();
          $personalized_para = $node->get($field_name)->referencedEntities();
          if($personalized_para == null) {
            //Paragraphs never created and saved a personalized_paragraph.
            \Drupal::logger('personalized_paragraphs')->notice("The node: ".$node->id()."has a personalized paragraph (PP) and was saved, but no PP was created");
          }else {
            $personalized_para = $personalized_para[0];
          }
          if(!$node->isDefaultRevision()){
            //A drafted node was saved
            $this->getDecisionStorage()->setnewRevision();
          }
          $saved_decision = $this->getDecisionStorage()->save();
          $personalized_para->set('field_decision_content_token', $saved_decision->getDecision()->getToken());
          $personalized_para->save();
          if ($saved_decision instanceof RevisionableParentEntityUsageInterface) {
            $has_usage = $saved_decision->getUsage();
            if(!empty($has_usage)){
              $saved_decision->deleteUsage();
            }
            $saved_decision->addUsage($personalized_para);
          }
        }
      }
    }

  }

  /**
   * Get the decision storage plugin.
   *
   * @return \Drupal\smart_content\Decision\Storage\DecisionStorageInterface
   *   The decision storage plugin.
   */
  public function getDecisionStorage() {
//    $caller = debug_backtrace()[1]['function'];
//    if($caller == 'buildWidget'){
//      dd($this->decisionStorage);
//    }
    if (!isset($this->decisionStorage)) {
      $decision_storage_configuration = $this->getConfiguration()['decision_storage'];
      $storage_plugin_id = isset($decision_storage_configuration['plugin_id']) ? $decision_storage_configuration['plugin_id'] : 'content_entity';
      $this->decisionStorage = $this->decisionStorageManager
        ->createInstance($storage_plugin_id, (array) $decision_storage_configuration);
    }
    return $this->decisionStorage;
  }


}
