<?php
namespace Drupal\personalized_paragraphs\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Paragraph handler item annotation object.
 *
 * @see \Drupal\personalized_paragraphs\Plugin\PersonalizedParagraphsManager
 * @see plugin_api
 *
 * @Annotation
 */
class PersonalizedParagraph extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
